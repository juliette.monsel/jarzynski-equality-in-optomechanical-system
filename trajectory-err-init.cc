#include <iostream>
#include <string>
#include <complex>
#include <cmath>
#include <random>
#include <thread>
#include "cnpy.h"

using namespace std;

const unsigned int thread_nb (8);          // number of threads to use
const unsigned int nb (5000000/thread_nb); // number of trajectories

// Constants
const double pi (3.141592653589793115997963468544185161590576171875);
const double h (6.62607004e-34/(2*pi));

//Params
const double T (80);                    // mechanical temperature T (K)
const double hb (h/(1.38064852e-23*T));
const double w0 (1.2/hb);               // TLS frequency (rad/s)
const double Om (2*pi*1e5);             // mechanical frequency (rad/s)
const double gam (5*Om);                // spontaneous emission rate \gamma
const double dt (5e-5/(gam));           // time step (s)
const double gm (10*Om);                // coupling strength

const unsigned long N ((ceil(pi/(dt*2*Om))) + 1);  // number of time steps

// precomputed quantities
const double dgm (2*gm);
const double hdgm (h*dgm);
const complex<double> iOmdt (0, -Om*dt), gmOm (gm/Om, 0);
const double gamdt (gam*dt);
const double p_e (1/(exp(hb*w0) + 1));  // proba for the TLS to be initially in |e>

void entropy (const double beta0, const double err, const unsigned int n)
{
    // filenames
    char W_name[50];
    char bf_name[50];
    char pr_name[50];
    char pd_name[50];

    bool Pe;
    complex<double> beta, beta_prec, bgmOm;
    unsigned int n0, dn, dN;
    double W, r, p_no_jump, pr_traj, pd_traj, s, gam_jump_dt, gam_jump_dt_r;

    // creation of the .npy files
    sprintf(pr_name, "pr_traj_beta0_%g_err_%g-%d.npy", beta0, err, n);
    sprintf(pd_name, "pd_traj_beta0_%g_err_%g-%d.npy", beta0, err, n);
    sprintf(W_name, "W_beta0_%g_err_%g-%d.npy", beta0, err, n);
    sprintf(bf_name, "bf_beta0_%g_err_%g-%d.npy", beta0, err, n);
    cnpy::npy_save(W_name, &W, {0}, "w");
    cnpy::npy_save(bf_name, &beta, {0}, "w");
    cnpy::npy_save(pr_name, &pr_traj, {0}, "w");
    cnpy::npy_save(pd_name, &pd_traj, {0}, "w");
    cout << W_name << endl;

    unsigned int j;
    for ( j = 0; j < nb; j++ )
    {
        Pe = ((double) rand() / (RAND_MAX)) < p_e;
        n0 = 0;
        beta = complex<double>(0, beta0);
        W = 0;
        pr_traj = 1;
        pd_traj = Pe*p_e + (1-Pe)*(1-p_e);
        gam_jump_dt = 1;
        gam_jump_dt_r = 1;

        while ( n0 < N )
        {
            // dn = n - n0 (dn*dt = t - t0)
            pr_traj *= gam_jump_dt_r;
            pd_traj *= gam_jump_dt;
            dn = 0;
            dN = N - n0;
            beta_prec = beta;
            r = ((double) rand() / (RAND_MAX));
            // propa for the jump to occur between n0 and n
            s = 0;
            p_no_jump = 1;
            if ( Pe )
            {
                bgmOm = (beta_prec + gmOm);
                // TLS in e -> jump = emission
                while ( dn < dN && r > s )
                {
                    beta = bgmOm*exp(iOmdt*double(dn)) - gmOm;
                    gam_jump_dt = gamdt*(1/(exp(hb*(w0 + dgm*beta.real())) - 1) + 1);
                    // propa for the emission to occur between n0 and n + 1
                    s += p_no_jump*gam_jump_dt;
                    // proba no jump before n + 1
                    p_no_jump *= exp(- gam_jump_dt);
                    dn++;
                }
                W += hdgm*real(beta - beta_prec);
                gam_jump_dt_r = gam_jump_dt - gamdt;
            }
            else
            {
                // TLS in g -> jump = absorption
                while ( dn < dN && r > s )
                {
                    beta = beta_prec*exp(iOmdt*double(dn));
                    gam_jump_dt = gamdt/(exp(hb*(w0 + dgm*beta.real())) - 1);
                    // propa for the emission to occur between n0 and n + 1
                    s += p_no_jump*gam_jump_dt;
                    // proba no jump before n + 1
                    p_no_jump *= exp(- gam_jump_dt);
                    dn++;
                }
                gam_jump_dt_r = gam_jump_dt + gamdt;
            }
            pr_traj *= p_no_jump;
            pd_traj *= p_no_jump;
            n0 += dn;
            Pe = ! Pe;
        }
        const double wN = w0 + dgm * beta.real();
        pr_traj *= exp(-hb * wN * (1 - Pe)) / (1 + exp(-hb * wN));
        cnpy::npy_save(W_name, &W, {1}, "a");
        cnpy::npy_save(bf_name, &beta, {1}, "a");
        cnpy::npy_save(pr_name, &pr_traj, {1}, "a");
        cnpy::npy_save(pd_name, &pd_traj, {1}, "a");
    }

}

int main (int argc, char *argv[]) {
    // argv[1] = beta0        initial mechanical amplitude |\beta_0|
    // argv[2] = err          initial error (\delta \beta in the article) [the final measurement error is applied during the data trreatment]
    const double beta0 (atof(argv[1]));
    const double err (atof(argv[2]));

    // random seed
    srand((int) random_device{}());
    char file_name[] = "params.npz";
    cnpy::npz_save(file_name, "T", &T, {1}, "w");
    cnpy::npz_save(file_name, "gam", &gam, {1}, "a");
    cnpy::npz_save(file_name, "dt", &dt, {1}, "a");
    cnpy::npz_save(file_name, "Om", &Om, {1}, "a");
    cnpy::npz_save(file_name, "gm", &gm, {1}, "a");
    cnpy::npz_save(file_name, "w0", &w0, {1}, "a");
    cnpy::npz_save(file_name, "beta0", &beta0, {1}, "a");
    cnpy::npz_save(file_name, "err", &err, {1}, "a");
    // time step check
    cout << gamdt*(1/(exp(hb*w0) - 1) + 1) << ' ' << Om*dt << ' ' << gm*dt<<endl;

    thread t[thread_nb];

    for ( unsigned int i=0; i < thread_nb; i++ )
    {
        t[i] = thread (entropy, beta0, err, i);
    }
    for ( unsigned int i=0; i < 8; i++ )
    {
        t[i].join();
    }

    return 0;
}
