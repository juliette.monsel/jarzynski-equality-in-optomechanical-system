# Jarzynski equality in optomechanical system

Code used to generate stochastic trajectories for the hybrid optomechanical system in the article

Monsel, J., Elouard, C. & Auffèves, A. An autonomous quantum machine to measure the thermodynamic arrow of time. *npj Quantum Inf* **4**, 59 (2018). https://doi.org/10.1038/s41534-018-0109-8

Please cite the above article if you use the code in a publication.

- trajectory.cc: direct trajectories without measurement error
- trajectory-err-init.cc: direct trajectories with measurement error on the initial state (the measurement error on the final state is obtained during the post-processing of the generated trajectories)

BibTex citation:

```
@article{Monsel2018Nov,
    title = {An Autonomous Quantum Machine to Measure the Thermodynamic Arrow of Time},
    author = {Monsel, Juliette and Elouard, Cyril and Auff{\`e}ves, Alexia},
    year = {2018},
    month = nov,
    volume = {4},
    pages = {59},
    doi = {10.1038/s41534-018-0109-8},
    journal = {Npj Quantum Inf.},
    number = {1}
}
```

Dataset from article: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10392426.svg)](https://doi.org/10.5281/zenodo.10392426)


Requirements
============

- g++ (compilation)
- cnpy (save data in .npy/.npz files) - https://github.com/rogersce/cnpy


Usage
=====

Parameters
----------

Edit the parameters in the file. Notations:


| Argument      | Description                                           | Notation in the article |
| ------------- | ------------------------------------------------------| ----------------------- |
| `beta0`       | Initial mechanical amplitude                          | \|\beta_0\|             |
| `T`           | Qubit's bath temperature in K                         | T                       |
| `h*w0/(kB*T)` | Qubit's frequency in units of \hbar/(k_B T)           | \omega_0                |
| `Om/(2pi)`    | Mechanical frequency in Hz                            | \Omega/2\pi             |
| `gam/Om`      | Spontaneous emission rate in units of \Omega          | \gamma                  |
| `gm/Om`       | Optomechanical coupling strength in units of \Omega   | \gamma                  |
| `dt`          | Time step in units of 1/\gamma                        | \Delta t                |

See the article for the notations and the domain of validity.



Compile
-------

    $ g++ trajectory.cc -o trajectory -Ofast -lz -lcnpy -lpthread


Run
---
    $ ./trajectory beta0 


Output
------

A `params.npz` archive containing the parameters and .npy files containing the quantities of interest:

+ The work in `W_beta0_<beta0>_T_<T>_w0_<w0/2pi>_Om_<Om/2pi>_gam_<gam/2pi>_gm_<gm/2pi>-<n>.npy` 
+ The final mechanical state \beta(t_f) in `bf_beta0_<beta0>_T_<T>_w0_<w0/2pi>_Om_<Om/2pi>_gam_<gam/2pi>_gm_<gm/2pi>-<n>.npy` 
+ The probability of the direct trajectory in `pd_traj_beta0_<beta0>_T_<T>_w0_<w0/2pi>_Om_<Om/2pi>_gam_<gam/2pi>_gm_<gm/2pi>-<n>.npy` 
+ The probability of the associated reversed trajectory in `pr_traj_beta0_<beta0>_T_<T>_w0_<w0/2pi>_Om_<Om/2pi>_gam_<gam/2pi>_gm_<gm/2pi>-<n>.npy` 

`n` is the file number between `0` and `thread_nb - 1`, where `thread_nb` is the hardcoded number of threads to use.
Each .npy file contains the values for `nb/thread_nb` trajectories where `nb` is the hardcoded total number of trajectories.


The data can easily be loaded and analyzed with python:

```python
import numpy as np
# load parameters
params = np.load("params.npz")
# list archive's conten
print(params.files)  
# ['T', 'gam', 'dt', 'Om', 'gm', 'w0']

thread_nb = 8
# final mechanical states
bf = np.concatenate(tuple(np.load("bf_...-%i.npy" % n) for n in range(thread_nb)))
# work
W = np.concatenate(tuple(np.load("W_...-%i.npy" % n) for n in range(thread_nb)))
```

Contact
=======

Juliette Monsel - monsel<at\>chalmers<dot\>se
